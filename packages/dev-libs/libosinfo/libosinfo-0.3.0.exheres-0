# Copyright 2012 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require pagure
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require udev-rules

SUMMARY="GObject based library API for managing information about operating systems"
HOMEPAGE="http://${PN}.org/"

LICENCES="LGPL-2"
SLOT="1.0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    vapi [[ requires = [ gobject-introspection ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.10] )
        vapi? ( dev-lang/vala:* )
    build+run:
        dev-libs/glib:2
        dev-libs/libxml2:2.0[>=2.6.0]
        dev-libs/libxslt[>=1.0.0]
        gnome-desktop/libsoup:2.4[>=2.42]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.7] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-do-not-hardcode-ld.patch
    "${FILES}"/0002-do-not-hardcode-nm.patch
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "gobject-introspection introspection"
    gtk-doc
    "vapi vala"
)

src_prepare() {
    edo intltoolize --automake --force --copy
    autotools_src_prepare
}

