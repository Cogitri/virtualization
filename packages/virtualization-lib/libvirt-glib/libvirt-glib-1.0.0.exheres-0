# Copyright 2012 Marc-Antoine Perennou<Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require python [ blacklist='3' with_opt='true' multibuild='true' ]

SUMMARY="GLib integration for libvirt"
DESCRIPTION="
libvirt-glib - GLib main loop integration & misc helper APIs
libvirt-gconfig - GObjects for manipulating libvirt XML documents
libvirt-gobject - GObjects for managing libvirt objects
"
HOMEPAGE="http://libvirt.org/"
DOWNLOADS="ftp://libvirt.org/libvirt/glib/${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc
    vapi
    ( linguas: bo cs de el es fr gl ja ka kn nl pl pt ru te tr uk vi zh_CN zh_TW )
"

# The tests are broken (0.2.1)
#RESTRICT="test"

DEPENDENCIES="
    build:
        gtk-doc? ( dev-doc/gtk-doc )
        vapi? ( dev-lang/vala:*[>=0.13] )
    build+run:
        dev-libs/glib:2[>=2.38.0]
        dev-libs/libxml2:2.0
        gnome-desktop/gobject-introspection:1[>=1.36.0]
        virtualization-lib/libvirt[>=1.2.8]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/0001-do-not-hardcode-nm.patch )

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( python )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    gtk-doc
    "vapi vala"
)

libvirt-glib_src_prepare() {
    default
    eautoreconf
}

