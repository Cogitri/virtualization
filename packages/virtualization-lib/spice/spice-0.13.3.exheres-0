# Copyright 2011-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist=3 multibuild=false ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="server for a remote-display system built for virtual environments"
DESCRIPTION="
The SPICE project aims to provide a complete open source solution for interaction
with virtualized desktop devices. It deals with both the virtualized devices and
the front-end. Interaction between front-end and back-end is done using VD-Interfaces.
The VD-Interfaces (VDI) enable both ends of the solution to be easily utilized by
a third-party component.
Adequate user experience and the lack of a good solution for virtual machine remote
access were what sparked the Spice project. To ensure that Spice is a success, the
following project goals were set:
a) To deliver a high-quality user experience, similar to local machine, in LAN environments
b) To maintain low CPU consumption in order to have high VM density on the host
c) To provide high-quality video streaming and 3D
"
HOMEPAGE="https://www.spice-space.org"
DOWNLOADS="${HOMEPAGE}/download/releases/${PNV}.tar.bz2"

BUGS_TO="philantrop@exherbo.org"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gstreamer
    lz4 [[ description = [ Enable lz4 compression ] ]]
    smartcard [[ description = [ Enable smartcard support ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
        dev-python/pyparsing[python_abis:*(-)?]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.22]
        media-libs/celt:0.5.1[>=0.5.1.1]
        media-libs/opus
        net-libs/cyrus-sasl
        virtualization-lib/spice-protocol[>=0.12.12]
        x11-libs/pixman:1[>=0.17.7]
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-libav
            media-plugins/gst-plugins-base:1.0
            media-plugins/gst-plugins-good:1.0[gstreamer_plugins:vpx]
            media-plugins/gst-plugins-ugly:1.0[gstreamer_plugins:h264]
        )
        lz4? ( app-arch/lz4[>=1.7.5] )
        smartcard? ( dev-libs/libcacard[>=2.5.1] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/48da2c6654f37fcf02711aa86a08d6404dd6d215.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-celt051
    --enable-manual
    --disable-automated-tests
    --disable-werror
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gstreamer gstreamer 1.0'
    lz4
    smartcard
)

